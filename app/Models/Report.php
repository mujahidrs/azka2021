<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice',
        'volunteer_id',
        'donor_id',
        'donation_id',
        'nominal',
        'period_id',
        'date',
        'type',
        'proof',
        'desc',
        'donation_to'
    ];

    public function donor()
    {
        return $this->hasOne(Donor::class, "id", "donor_id");
    }

    public function volunteer()
    {
        return $this->hasOne(User::class, "id", "volunteer_id");
    }

    public function donation()
    {
        return $this->hasOne(Donation::class, "id", "donation_id");
    }
}
