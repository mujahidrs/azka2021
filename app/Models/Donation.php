<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'sort_number',
        'status',
        'period_id'
    ];

    public function reports()
    {
    	return $this->belongsTo(Report::class);
    }
}
