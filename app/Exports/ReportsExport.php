<?php

namespace App\Exports;

use App\Models\Period;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class ReportsExport implements WithColumnFormatting, FromView, WithColumnWidths, WithEvents
{
	use Exportable;

	private $query;
    public $selected_period;
    public $export_date;
    public $reports;
    public $achievements;
    public $percentage;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($selected_period, $export_date, $reports, $achievements, $percentage)
    {
		$this->selected_period = $selected_period;
		$this->export_date = $export_date;
        $this->reports = $reports;
        $this->achievements = $achievements;
        $this->percentage = $percentage;
    }

    public function view(): View
    {
        $selected_period = Period::find($this->selected_period);
        $export_date = $this->export_date;
        $reports = $this->reports;
        $achievements = $this->achievements;
        $percentage = $this->percentage;
        
        return view("exports.reports", [
            "reports"=>$reports, 
            "export_date"=>$export_date, 
            "selected_period"=>$selected_period,
            "achievements"=>$achievements,
            "percentage"=>$percentage,
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 30/6,    //No
            'B' => 100/6,   //Donation Date
            'C' => 200/6,   //Donor Name
            'D' => 100/6,   //Donor Phone
            'E' => 150/6,   //Donor Email
            'F' => 150/6,   //Donor NPWP
            'G' => 150/6,   //Volunteer Name
            'H' => 100/6,   //Donation Type
            'I' => 100/6,   //Donation To
            'J' => 100/6,   //Account Number
            'K' => 125/6,   //Invoice
            'L' => 200/6,   //Donation Name
            'M' => 100/6,   //Nominal
            'N' => 100/6,   //Proof of Payment
            'O' => 200/6,   //Donor Address
            'P' => 150/6,   //Partnership Code
        ];
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_NUMBER,
            'J' => NumberFormat::FORMAT_NUMBER,
            'K' => NumberFormat::FORMAT_NUMBER,
            'M' => NumberFormat::FORMAT_NUMBER,
        ];
    }

    public function registerEvents(): array
    {
    	return [
    		AfterSheet::class => function(AfterSheet $event){
    			$reports = $this->reports;
                $startRow = 7;
		        $lastRow = count($reports) + $startRow;
                $lastColumn = "P";
		  
                //Membuat header table tebal
    			$event->sheet->getStyle('A'. $startRow . ':' . $lastColumn . $startRow)->applyFromArray([
    				'font' => [
    					'bold' => true
                    ],
    			]);

                //Menjadikan seluruh isi table memiliki border hitam dengan alignment center
    			$event->sheet->getStyle('A'. $startRow . ':' . $lastColumn . $lastRow)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
         		]);

                //Membuat kolom Donor Name alignment kiri tengah
                $event->sheet->getStyle('C'. $startRow .':' . 'C' . $lastRow)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);

                //Membuat kolom Volunteer Name alignment kiri tengah
                $event->sheet->getStyle('G'. $startRow+1 .':' . 'G' . $lastRow)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);

                //Membuat kolom Nominal alignment kanan tengah
                $event->sheet->getStyle('M'. $startRow+1 .':' . 'M' . $lastRow)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);

                //Menjadikan kolom Donor Address alignment kiri tengah
                $event->sheet->getStyle('O'. $startRow+1 .':' . 'O' . $lastRow)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);

                // $event->sheet->getStyle('J'. $startRow .':' . $lastColumn . $lastRow)->applyFromArray([
                //     'alignment' => [
                //         'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                //     ],
                // ]);

                //Menjadikan kolom Invoice alignment kanan tengah
                $event->sheet->getStyle('K'. $startRow+1 .':' . 'K' . $lastRow)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);

                // $event->sheet->getStyle('K'. $startRow .':' . 'K' . $lastRow)->applyFromArray([
                //     'alignment' => [
                //         'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                //     ],
                // ]);
                
                
                // $event->sheet->getStyle('A'. $startRow . ':' . $lastColumn . $startRow)->applyFromArray([
                //     'alignment' => [
                //         'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                //         'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                //     ],
    			// ]);
    		}
    	];
    }
}
