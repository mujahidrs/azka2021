<?php

namespace App\Exports;

use App\Models\Period;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Illuminate\Contracts\View\View;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class ResumeExport implements WithColumnFormatting, FromView, WithColumnWidths, WithEvents
{
	use Exportable;

	private $query;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($selected_period, $export_date, $donations, $achievements, $percentage)
    {
		$this->selected_period = $selected_period;
		$this->export_date = $export_date;
        $this->donations = $donations;
        $this->achievements = $achievements;
        $this->percentage = $percentage;
    }

    public function view(): View
    {
        $selected_period = Period::find($this->selected_period);
        $export_date = $this->export_date;
        $donations = $this->donations;
        $achievements = $this->achievements;
        $percentage = $this->percentage;
        
        return view("exports.resume", [
            "donations"=>$donations, 
            "export_date"=>$export_date, 
            "selected_period"=>$selected_period,
            "achievements"=>$achievements,
            "percentage"=>$percentage,
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 30/6,
            'B' => 250/6,
            'C' => 150/6,
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER,
        ];
    }

    public function registerEvents(): array
    {
    	return [
    		AfterSheet::class => function(AfterSheet $event){
    			$donations = $this->donations;
                $startRow = 7;
		        $lastRow = count($donations) + $startRow;
                $lastColumn = "C";
		  
    			$event->sheet->getStyle('A'. $startRow . ':' . $lastColumn . $startRow)->applyFromArray([
    				'font' => [
    					'bold' => true
                    ],
    			]);
    			$event->sheet->getStyle('A'. $startRow . ':' . $lastColumn . $lastRow)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['argb' => '000000'],
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
         		]);
                 $event->sheet->getStyle('B'. $startRow .':' . 'B' . $lastRow)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('C'. $startRow .':' . 'C' . $lastRow)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
                ]);
                $event->sheet->getStyle('A'. $startRow . ':' . $lastColumn . $startRow)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    ],
    			]);
    		}
    	];
    }
}
