<?php

namespace App\Http\Controllers;

use App\Models\Donation;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Period;
use App\Models\Report;
use App\Models\Target;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $now = date('Y-m-d H:i');
        $emergency = date('Y-m-d H:i', strtotime("+1 month", strtotime($now)));

        $user_role = User::withTrashed()->find(Auth::user()->id)->role;

        $volunteers = User::withTrashed()->where('role_id', 3)->get();
        $trashed_volunteers = User::onlyTrashed()->where('role_id', 3)->get();

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $donations = Donation::all();

        if($selected_period === null){
            Period::create([
                'name' => "Emergency",
                'start_date' => $now,
                'end_date' => $emergency,
                'total_target' => 0,
            ]);

            foreach($donations as $key => $donation){
                Donation::find($donation->id)->update([
                    "status" => "inactive"
                ]);
            }

            return redirect('home');
        }
        else{
            if(Auth::user()->role_id == 3) {
                $level_id = User::withTrashed()->find(Auth::user()->id)->class?->level_id;
            } else {
                $level_id = 0;
            }
    
            $target = Target::where('period_id', $selected_period->id)->where('level_id', $level_id)->pluck('target')->first();
    
            $user = (object) [
                'rank' => 0,
                'count' => 0,
                'achievement' => 0,
                'transaction' => 0,
                'progress' => 0
            ];
    
            foreach($volunteers as $key => $volunteer) {
                $volunteer->achievement = Report::select(DB::raw('*, SUM(nominal) as sum_nominal'))
                                            ->where('period_id', $selected_period->id)
                                            ->where('volunteer_id', $volunteer->id)
                                            ->groupBy('volunteer_id')
                                            ->orderBy('sum_nominal', 'ASC')
                                            ->pluck('sum_nominal')
                                            ->first();
    
                $volunteer->transaction = Report::where('nominal', '>', 0)
                                            ->where('period_id', $selected_period->id)
                                            ->where('volunteer_id', $volunteer->id)
                                            ->count();
    
                if(Auth::user()->role_id == 3){
                    if($volunteer->id == Auth::user()->id) {
                        $user->rank = $key+1;
                        $user->achievement = $volunteer->achievement;
                        $user->transaction = $volunteer->transaction;
                    };
    
                    $page = 'home';
                } else {
                    $user->count = $volunteers->count() - $trashed_volunteers->count();
                    $user->achievement += $volunteer->achievement;
                    $user->transaction += $volunteer->transaction;
    
                    $page = 'home-admin';
                }
            };
    
            if($target > 0) {
                $user->progress = $user->achievement / $target * 100;
            } else{
                $user->progress = 0;
            }
    
        if(Auth::user()->role_id == 3){
            $sorted = $volunteers->sortBy([['achievement', 'desc'], ['transaction', 'desc'], ['name', 'asc']])->values();
    
            foreach($sorted as $index => $result){
                if($result->id == Auth::user()->id){
                    $user->rank = $index + 1;
                }
            }
        }
    
            return view($page, [
                'user_role' => $user_role,
                'periods' => $periods,
                'selected_period' => $selected_period,
                'target' => $target,
                'volunteers' => $volunteers,
                'user' => $user
            ]);
        }
        }
}
