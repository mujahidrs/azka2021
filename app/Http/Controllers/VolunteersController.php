<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\Classes;
use Illuminate\Support\Facades\Hash;

class VolunteersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where("role_id", 3)->orderBy("created_at", "DESC")->get();
        $roles = Role::all();
        $classes = Classes::all();

        return view('volunteers', [
            'users' => $users,
            'roles' => $roles,
            'classes' => $classes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->phone = '62' . $request->phone;
        $request->password = Hash::make($request->password);

        $volunteer_role = Role::create(["name"=>"Volunteer"]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'username' => $request->username,
            'gender' => $request->gender,
            'phone' => $request->phone,
            "role_id"=>$volunteer_role->id,
            'class_id' => $request->class_id
        ]);

        return redirect()->back()->with("status", "Add User Success");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->phone = '62' . $request->phone;

        User::find($id)->update($request->all());

        return redirect()->back()->with("status", "Edit User Success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();

        return redirect()->back()->with("status", "Delete User Success");
    }
}
