<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\Period;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\Target;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade\Pdf as PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = date('Y-m-d H:i');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $level_id = User::find(Auth::user()->id)->class?->level_id;

        $target = Target::where('period_id', $selected_period->id)->where('level_id', $level_id)->pluck('target')->first();

        $reports = Report::select(DB::raw('*, SUM(nominal) as sum_nominal'))
                    ->where("volunteer_id", Auth::user()->id)
                    ->where("period_id", $selected_period->id)
                    ->groupBy('invoice')
                    ->orderBy("created_at", "DESC")
                    ->get();

        return view('reports', [
            'reports' => $reports,
            'periods' => $periods,
            'selected_period' => $selected_period,
            'target' => $target,
        ]);
    }

    public function filterByPeriod(Request $request)
    {
        $periods = Period::all();

        $selected_period = Period::find($request->period_id);

        $level_id = User::find(Auth::user()->id)->class->level_id;

        $target = Target::where('period_id', $selected_period->id)->where('level_id', $level_id)->pluck('target')->first();

        $reports = Report::select(DB::raw('*, SUM(nominal) as sum_nominal'))
                    ->where("volunteer_id", Auth::user()->id)
                    ->where("period_id", $selected_period->id)
                    ->groupBy('invoice')
                    ->orderBy("created_at", "DESC")
                    ->get();

        return view('reports', [
            'reports' => $reports,
            'periods' => $periods,
            'selected_period' => $selected_period,
            'target' => $target,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $now = date('Y-m-d H:i');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $donations = Donation::where("status", "active")
                        ->where("period_id", $selected_period->id)
                        ->orderBy("sort_number", "ASC")->get();
                        
        $reports = Report::where("volunteer_id", Auth::user()->id)
                        ->groupBy("donor_id")
                        ->get();

        foreach ($reports as $key => $report) {
            $report->donor = $report->donor;
        }

        return view('report', [
            'donations' => $donations,
            'reports' => $reports,
            'periods' => $periods,
            'selected_period' => $selected_period
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $sum_nominal = 0;

        foreach ($request->nominal as $nominal) {
            $sum_nominal += $nominal;
        }

        if($sum_nominal === 0){
            return redirect()->back()->with("status", "Report failed. You must fill in at least one donation!");
        }

        if($request->donor_type == "new"){
            $validated = $request->validate([
                'name' => 'required|string',
                'date' => 'required',
                'proof' => 'required',
                'donation_to' => 'required'
            ]);

            $donor = Donor::create([
                'name' => $request->name,
                'address' => $request->address,
                'phone' => $request->phone,
                'npwp' => $request->npwp,
                'email' => $request->email,
            ]);

            $donor_id = $donor->id;
        }
        if($request->donor_type == "saved"){
            $validated = $request->validate([
                'donor_id' => 'required|string',
                'date' => 'required',
                'proof' => 'required',
                'donation_to' => 'required'
            ]);

            $donor_id = $request->donor_id;
        }

        $invoice = Carbon::now()->format('YmdHis').$request->volunteer_id.$request->period_id;

        $path = null;

        if(isset($request->proof)){
            $pathName = pathinfo($request->proof->getClientOriginalName())['filename'];
            $pathExtension = $request->proof->getClientOriginalExtension();
            $path = '/uploads/proofs/' . $invoice . '.' . $pathExtension;
        }

        foreach ($request->donation_id as $key => $donation_id) {
            Report::create([
                'invoice' => $invoice,
                'volunteer_id' => $request->volunteer_id,
                'donor_id' => $donor_id,
                'donation_id' => $donation_id,
                'nominal' => $request->nominal[$key],
                'period_id' => $request->period_id,
                'date' => $request->date,
                'type' => $request->type,
                'proof' => $path,
                'desc' => $request->desc[$key],
                'donation_to' => $request->donation_to
            ]);
        }

        if(isset($request->proof)){
            $request->proof->move(public_path() . '/uploads/proofs/', $path);
        }

        return redirect('reports')->with("status", "Donation Report Success");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $now = date('Y-m-d H:i');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $donations = Donation::where("status", "active")->orderBy("sort_number", "ASC")->get();

        $reports = Report::where("invoice", $id)->get();

        return view('show_report', [
            'donations' => $donations,
            'periods' => $periods,
            'selected_period' => $selected_period,
            'reports' => $reports,
        ]);
    }

    public function download($id)
    {
        $now = date('Y-m-d H:i');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $donations = Donation::where("status", "active")->orderBy("sort_number", "ASC")->get();

        $reports = Report::where("invoice", $id)->get();

        $url = url('/receipt/' . $reports[0]->invoice);

        $qr_code = QrCode::format('png')
            ->size('100')
            ->generate($url, public_path('images/qrcode-' . $reports[0]->invoice . '.png'));

        $file_qr = 'qrcode-' . $reports[0]->invoice . '.png';

        $data = [
            'donations' => $donations,
            'periods' => $periods,
            'selected_period' => $selected_period,
            'reports' => $reports,
            'file_qr' => $file_qr,
        ];
          
        $pdf = PDF::loadView('receipt', $data)->setPaper('a4', 'landscape')->setOptions(['dpi' => 160, 'defaultFont' => 'sans-serif', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);

        $pdf->getDomPDF()->setHttpContext(
            stream_context_create([
                'ssl' => [
                    'allow_self_signed'=> TRUE,
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                ]
            ])
        );

        $filename = "receipt-" . $reports[0]->invoice . ".pdf";
    
        return $pdf->download($filename);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $now = date('Y-m-d H:i');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $donations = Donation::where("status", "active")->orderBy("sort_number", "ASC")->get();

        $reports = Report::where("invoice", $id)->get();

        return view('edit_report', [
            'donations' => $donations,
            'periods' => $periods,
            'selected_period' => $selected_period,
            'reports' => $reports,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'date' => 'required',
        ]);

        $sum_nominal = 0;

        foreach ($request->nominal as $nominal) {
            $sum_nominal += $nominal;
        }

        if($sum_nominal === 0){
            return redirect()->back()->with("status", "Edit Report failed. You must fill in at least one donation!");
        }

        $donor = Donor::find($request->donor_id)->update([
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'npwp' => $request->npwp,
            'email' => $request->email,
        ]);

        $invoice = $request->invoice;

        $path = null;

        if(isset($request->proof)){
            $pathName = pathinfo($request->proof->getClientOriginalName())['filename'];
            $pathExtension = $request->proof->getClientOriginalExtension();
            $path = '/uploads/proofs/' . $invoice . '.' . $pathExtension;
        }

        if($path != null){
            foreach ($request->donation_id as $key => $donation_id) {
                Report::where('invoice', $invoice)->where('donation_id', $donation_id)->update([
                    'invoice' => $invoice,
                    'volunteer_id' => $request->volunteer_id,
                    'donor_id' => $request->donor_id,
                    'donation_id' => $donation_id,
                    'nominal' => $request->nominal[$key],
                    'period_id' => $request->period_id,
                    'date' => $request->date,
                    'type' => $request->type,
                    'proof' => $path,
                    'desc' => $request->desc[$key],
                ]);
            }
        }
        else{
            foreach ($request->donation_id as $key => $donation_id) {
                Report::where('invoice', $invoice)->where('donation_id', $donation_id)->update([
                    'invoice' => $invoice,
                    'volunteer_id' => $request->volunteer_id,
                    'donor_id' => $request->donor_id,
                    'donation_id' => $donation_id,
                    'nominal' => $request->nominal[$key],
                    'period_id' => $request->period_id,
                    'date' => $request->date,
                    'type' => $request->type,
                    'desc' => $request->desc[$key],
                ]);
            }
        }
        

        if(isset($request->proof)){
            $request->proof->move(public_path() . '/uploads/proofs/', $path);
        }

        return redirect('reports')->with("status", "Edit Donation Report Success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $donor_id = Report::where("invoice", $id)->pluck("donor_id")->first();

        Report::where("invoice", $id)->delete();

        Donor::find($donor_id)->delete();

        return redirect()->back()->with("status", "Delete Report Success");
    }
}
