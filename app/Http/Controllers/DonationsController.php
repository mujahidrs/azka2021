<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Donation;
use App\Models\Period;

class DonationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = date('Y-m-d H:i');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $donations = Donation::orderBy("sort_number", "ASC")->where("period_id", $selected_period->id)->get();

        return view('donations', [
            'periods' => $periods,
            'selected_period' => $selected_period,
            'donations' => $donations
        ]);
    }

    public function filterByPeriod(Request $request)
    {
        $periods = Period::all();

        $selected_period = Period::find($request->period_id);

        $donations = Donation::orderBy("sort_number", "ASC")->where("period_id", $selected_period->id)->get();

        return view('donations', [
            'periods' => $periods,
            'selected_period' => $selected_period,
            'donations' => $donations
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $last_number = Donation::where('period_id', $request->period_id)->orderBy("sort_number", "DESC")->pluck("sort_number")->first();

        Donation::create([
            "name" => $request->name,
            "status" => "active",
            "sort_number" => $last_number + 1,
            "period_id" => $request->period_id
        ]);

        return redirect()->back()->with("status", "Add Donation Success");
    }

    public function duplicate(Request $request)
    {
        if($request->duplicate_from){
            $donations_to_duplicate = Donation::where('period_id', $request->duplicate_from)->get();

            foreach($donations_to_duplicate as $donation){
                Donation::create([
                    'name' => $donation->name,
                    'sort_number' => $donation->sort_number,
                    'status' => $donation->status,
                    'period_id' => $request->duplicate_to
                ]);
            }

            return redirect()->back()->with("status", "Duplicate Donations Success");
        }
        else{
            return redirect()->back()->with("status", "Duplicate Donations Failed");
        }
        
    }

    public function activateAll($id)
    {
        $donations = Donation::where("period_id", $id)->get();

        foreach($donations as $donation) {
            Donation::find($donation->id)->update([
                "status" => "active"
            ]);
        }

        return redirect()->back()->with("status", "Activate All Donation Success");
    }

    public function disableAll($id)
    {
        $donations = Donation::where("period_id", $id)->get();

        foreach($donations as $donation) {
            Donation::find($donation->id)->update([
                "status" => "inactive"
            ]);
        }

        return redirect()->back()->with("status", "Disable All Donation Success");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Donation::find($id)->update($request->all());

        return redirect()->back()->with("status", "Edit Donation Success");
    }

    public function turnOff($id)
    {
        Donation::find($id)->update([
            "status" => "inactive"
        ]);

        return redirect()->back()->with("status", "Turn Off Donation Success");
    }

    public function turnOn($id)
    {
        Donation::find($id)->update([
            "status" => "active"
        ]);

        return redirect()->back()->with("status", "Turn On Donation Success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Donation::find($id)->delete();

        return redirect()->back()->with("status", "Delete Donation Success");
    }
}
