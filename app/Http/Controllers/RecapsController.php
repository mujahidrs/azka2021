<?php

namespace App\Http\Controllers;

use App\Exports\ReportsExport;
use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\Period;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\Target;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade\Pdf as PDF;
use Maatwebsite\Excel\Facades\Excel;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class RecapsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = date('Y-m-d H:i');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        if(Auth::user()->role_id == 3) {
            $level_id = User::find(Auth::user()->id)->class->level_id;
        } else {
            $level_id = 0;
        }

        $target = $selected_period->total_target;

        $reports = Report::select(DB::raw('*, SUM(nominal) as sum_nominal'))
                    ->with('volunteer')
                    ->where("period_id", $selected_period->id)
                    ->groupBy('invoice')
                    ->orderBy("created_at", "DESC")
                    ->get();

        // dd($reports[3]->volunteer()->withTrashed()->first());

        return view('recaps', [
            'reports' => $reports,
            'periods' => $periods,
            'selected_period' => $selected_period,
            'target' => $target,
        ]);
    }

    public function filterByPeriod(Request $request)
    {
        $periods = Period::all();

        $selected_period = Period::find($request->period_id);

        if(Auth::user()->role_id == 3) {
            $level_id = User::find(Auth::user()->id)->class->level_id;
        } else {
            $level_id = 0;
        }

        $target = $selected_period->total_target;

        $reports = Report::select(DB::raw('*, SUM(nominal) as sum_nominal'))
                    ->where("period_id", $selected_period->id)
                    ->groupBy('invoice')
                    ->orderBy("created_at", "DESC")
                    ->get();

        return view('recaps', [
            'reports' => $reports,
            'periods' => $periods,
            'selected_period' => $selected_period,
            'target' => $target,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $now = date('Y-m-d');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $donations = Donation::where("status", "active")->orderBy("sort_number", "ASC")->get();

        return view('report', [
            'donations' => $donations,
            'periods' => $periods,
            'selected_period' => $selected_period
        ]);
    }

    public function export(Request $request) 
    {
        $export_date = date("d-m-Y h:i A");
        $selected_period = $request->period_id;
        $reports = Report::where("period_id", $selected_period)->where('nominal', '>', 0)->get();

        $period = Period::find($selected_period);

        $achievements = 0;

        foreach($reports as $report){
            $achievements += (int) $report->nominal;
        }

        if($period->total_target > 0){
            $percentage = $achievements / $period->total_target * 100;
        }
        else{
            $percentage = 0;
        }

        $filename = "Recaps_" . str_replace('/', 'or', $period->name)  . "_" . $export_date . ".xlsx";

        return (new ReportsExport($selected_period, $export_date, $reports, $achievements, $percentage))->download($filename);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'date' => 'required',
            'proof' => 'required'
        ]);

        $sum_nominal = 0;

        foreach ($request->nominal as $nominal) {
            $sum_nominal += $nominal;
        }

        if($sum_nominal === 0){
            return redirect()->back()->with("status", "Report failed. You must fill in at least one donation!");
        }

        $donor = Donor::create([
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'npwp' => $request->npwp,
            'email' => $request->email,
        ]);

        $invoice = Carbon::now()->format('YmdHis').$request->volunteer_id.$request->period_id;

        $path = null;

        if(isset($request->proof)){
            $pathName = pathinfo($request->proof->getClientOriginalName())['filename'];
            $pathExtension = $request->proof->getClientOriginalExtension();
            $path = '/uploads/proofs/' . $invoice . '.' . $pathExtension;
        }

        foreach ($request->donation_id as $key => $donation_id) {
            Report::create([
                'invoice' => $invoice,
                'volunteer_id' => $request->volunteer_id,
                'donor_id' => $donor->id,
                'donation_id' => $donation_id,
                'nominal' => $request->nominal[$key],
                'period_id' => $request->period_id,
                'date' => $request->date,
                'type' => $request->type,
                'proof' => $path,
                'desc' => $request->desc[$key],
            ]);
        }

        if(isset($request->proof)){
            $request->proof->move(public_path() . '/uploads/proofs/', $path);
        }

        return redirect('reports')->with("status", "Donation Report Success");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $now = date('Y-m-d');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $donations = Donation::where("status", "active")->orderBy("sort_number", "ASC")->get();

        $reports = Report::where("invoice", $id)->get();

        return view('show_report', [
            'donations' => $donations,
            'periods' => $periods,
            'selected_period' => $selected_period,
            'reports' => $reports,
        ]);
    }

    public function download($id)
    {
        $now = date('Y-m-d');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $donations = Donation::where("status", "active")->orderBy("sort_number", "ASC")->get();

        $reports = Report::where("invoice", $id)->get();

        $url = url('/receipt/' . $reports[0]->invoice);

        $qr_code = QrCode::format('png')
            ->size('100')
            ->generate($url, public_path('images/qrcode-' . $reports[0]->invoice . '.png'));

        $file_qr = 'qrcode-' . $reports[0]->invoice . '.png';

        $data = [
            'donations' => $donations,
            'periods' => $periods,
            'selected_period' => $selected_period,
            'reports' => $reports,
            'file_qr' => $file_qr,
        ];
          
        $pdf = PDF::loadView('receipt', $data)->setPaper('a4', 'landscape')->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);

        $pdf->getDomPDF()->setHttpContext(
            stream_context_create([
                'ssl' => [
                    'allow_self_signed'=> TRUE,
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                ]
            ])
        );

        $filename = "receipt-" . $reports[0]->invoice . ".pdf";
    
        return $pdf->download($filename);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $now = date('Y-m-d');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $donations = Donation::where("status", "active")->orderBy("sort_number", "ASC")->get();

        $reports = Report::where("invoice", $id)->get();

        return view('edit_report', [
            'donations' => $donations,
            'periods' => $periods,
            'selected_period' => $selected_period,
            'reports' => $reports,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'date' => 'required',
        ]);

        $sum_nominal = 0;

        foreach ($request->nominal as $nominal) {
            $sum_nominal += $nominal;
        }

        if($sum_nominal === 0){
            return redirect()->back()->with("status", "Edit Report failed. You must fill in at least one donation!");
        }

        $donor = Donor::find($request->donor_id)->update([
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'npwp' => $request->npwp,
            'email' => $request->email,
        ]);

        $invoice = $request->invoice;

        $path = null;

        if(isset($request->proof)){
            $pathName = pathinfo($request->proof->getClientOriginalName())['filename'];
            $pathExtension = $request->proof->getClientOriginalExtension();
            $path = '/uploads/proofs/' . $invoice . '.' . $pathExtension;
        }

        if($path != null){
            foreach ($request->donation_id as $key => $donation_id) {
                Report::where('invoice', $invoice)->where('donation_id', $donation_id)->update([
                    'invoice' => $invoice,
                    'volunteer_id' => $request->volunteer_id,
                    'donor_id' => $request->donor_id,
                    'donation_id' => $donation_id,
                    'nominal' => $request->nominal[$key],
                    'period_id' => $request->period_id,
                    'date' => $request->date,
                    'type' => $request->type,
                    'proof' => $path,
                    'desc' => $request->desc[$key],
                ]);
            }
        }
        else{
            foreach ($request->donation_id as $key => $donation_id) {
                Report::where('invoice', $invoice)->where('donation_id', $donation_id)->update([
                    'invoice' => $invoice,
                    'volunteer_id' => $request->volunteer_id,
                    'donor_id' => $request->donor_id,
                    'donation_id' => $donation_id,
                    'nominal' => $request->nominal[$key],
                    'period_id' => $request->period_id,
                    'date' => $request->date,
                    'type' => $request->type,
                    'desc' => $request->desc[$key],
                ]);
            }
        }
        

        if(isset($request->proof)){
            $request->proof->move(public_path() . '/uploads/proofs/', $path);
        }

        return redirect('reports')->with("status", "Edit Donation Report Success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Report::find($id)->delete();

        return redirect()->back()->with("status", "Delete Report Success");
    }
}
