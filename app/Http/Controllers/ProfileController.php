<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Classes;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
    	$user = Auth::user();
		$classes = Classes::all();

    	return view("profile", [
    		"user" => $user,
			"classes" => $classes
    	]);
    }

    public function changeName(Request $request)
    {
    	if($request->name !== null){
    		$name = $request->name;
	        $user = User::find(Auth::user()->id);

	        $user->update([
	    		"name"=>$name,
	    	]);

	    	return redirect()->back()->with("status", "Change Name Success");
    	}

        return redirect()->back()->with("status", "Change Name Failed");
    }

    public function changeUsername(Request $request)
    {
    	if($request->username !== null){
	    	$username = $request->username;
	        $user = User::find(Auth::user()->id);

	        $user->update([
	    		"username"=>$username,
	    	]);

	        return redirect()->back()->with("status", "Change Username Success");
	    }

	    return redirect()->back()->with("status", "Change Username Failed");
    }

	public function changeGender(Request $request)
    {
    	if($request->gender !== null){
	    	$gender = $request->gender;
	        $user = User::find(Auth::user()->id);

	        $user->update([
	    		"gender"=>$gender,
	    	]);

	        return redirect()->back()->with("status", "Change Gender Success");
	    }

	    return redirect()->back()->with("status", "Change Gender Failed");
    }

    public function changePassword(Request $request)
    {
    	$password = $request->password;
    	$password_confirmation = $request->password_confirmation;

    	if($request->password !== null && $password === $password_confirmation){
    		$user = User::find(Auth::user()->id);

	        $user->update([
	    		"password"=>Hash::make($password),
	    	]);

	        return redirect()->back()->with("status", "Change Password Success");
    	}

        return redirect()->back()->with("status", "Change Password Failed");
    }

    public function changePhone(Request $request)
    {
    	if($request->phone !== null){
	    	$phone = '62' . $request->phone;
	        $user = User::find(Auth::user()->id);

	        $user->update([
	    		"phone"=>$phone,
	    	]);

	        return redirect()->back()->with("status", "Change Phone Success");
    	}

    	return redirect()->back()->with("status", "Change Phone Failed");
    }

    public function changeEmail(Request $request)
    {
    	if($request->email !== null){
	        $email = $request->email;

	        $user = User::find(Auth::user()->id);

	        $user->update([
	    		"email"=>$email,
	    		"email_verified_at"=>null
	    	]);

	        $user->sendEmailVerificationNotification();

	        return redirect()->back()->with("status", "Change Email Success");
    	}

    	return redirect()->back()->with("status", "Change Email Failed");
    }

	public function changePartnershipCode(Request $request)
    {
    	if($request->class_id !== null){
	    	$class_id = $request->class_id;
	        $user = User::find(Auth::user()->id);

	        $user->update([
	    		"class_id"=>$class_id,
	    	]);

	        return redirect()->back()->with("status", "Change Partnership Code Success");
	    }

	    return redirect()->back()->with("status", "Change Partnership Code Failed");
    }

	public function changeIsConfirmed(Request $request)
    {
		$user = User::find(Auth::user()->id);

    	if($user->class_id !== null){
	        $user->update([
	    		"is_confirmed"=>1,
	    	]);

	        return redirect()->back()->with("status", "Change Is Confirmed Success");
	    }

	    return redirect()->back()->with("status", "Change Is Confirmed Failed");
    }
}
