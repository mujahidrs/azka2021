<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\Period;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\Target;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade\Pdf as PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class CertificateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::where("volunteer_id", Auth::user()->id)
                        ->groupBy('donor_id')
                        ->get();

        return view('certificate', [
            "reports" => $reports
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $name = $request->name;

        return view('show_certificate', [
            "name" => $name
        ]);
    }

    public function download(Request $request) {
        $nama_lengkap_donatur = $request->donor_name;
        $font_size            = $request->font_size;

        $invoice = Carbon::now()->format('ymdHis').Auth::user()->id;
        // $filename = "certificate-" . $report->invoice . ".pdf";

        // $html2pdf = new HTML2PDF('L', 'A4', 'en', false, 'ISO-8859-15', [0, 0, 0, 0]);
        // $html2pdf->setDefaultFont('times');
        // $html2pdf->writeHTML(view('sertif.view')->with('report', $report)->with('nama_lengkap_donatur', $nama_lengkap_donatur)->with('font_size', $font_size), isset($_GET['vuehtml']));
        // $html2pdf->Output($filename, 'D');

        $data = [
            'nama_lengkap_donatur' => $nama_lengkap_donatur,
            'font_size' => $font_size,
            'invoice' => $invoice,
        ];
          
        $pdf = PDF::loadView('view_certificate', $data)->setPaper('a4', 'landscape')->setOptions(['defaultFont' => 'times', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);

        $pdf->getDomPDF()->setHttpContext(
            stream_context_create([
                'ssl' => [
                    'allow_self_signed'=> TRUE,
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                ]
            ])
        );

        $filename = "certificate-" . $invoice . ".pdf";
    
        return $pdf->download($filename);
    }
}
