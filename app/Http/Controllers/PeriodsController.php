<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Period;
use App\Models\Level;
use App\Models\Target;

class PeriodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periods = Period::orderBy("created_at", "DESC")->get();
        $levels = Level::all();

        return view('periods', [
            'periods' => $periods,
            'levels' => $levels
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->start_date = str_replace("T"," ",$request->start_date);
        $request->end_date = str_replace("T"," ",$request->end_date);

        $period = Period::create([
            "name" => $request->name,
            "start_date" => $request->start_date,
            "end_date" => $request->end_date,
            "total_target" => $request->total_target,
        ]);

        foreach ($request->level_id as $key => $level_id) {
            Target::create([
                "level_id" => $level_id,
                "period_id" => $period->id,
                "target" => $request->target[$key]
            ]);
        }

        return redirect()->back()->with("status", "Add Period & Set Target Success");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->start_date = str_replace("T"," ",$request->start_date);
        $request->end_date = str_replace("T"," ",$request->end_date);
        
        $period = Period::find($id)->update([
            "name" => $request->name,
            "start_date" => $request->start_date,
            "end_date" => $request->end_date,
            "total_target" => $request->total_target,
        ]);

        foreach ($request->level_id as $key => $level_id) {
            Target::where("period_id", $id)->where("level_id", $level_id)->update([
                "level_id" => $level_id,
                "period_id" => $id,
                "target" => $request->target[$key]
            ]);
        }

        return redirect()->back()->with("status", "Edit Period & Set Target Success");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Target::where("period_id", $id)->delete();
        Period::find($id)->delete();

        return redirect()->back()->with("status", "Delete Period & Target Success");
    }
}
