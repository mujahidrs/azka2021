<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes(['verify'=>true]);

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->middleware(['confirmed'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('/calculator', [App\Http\Controllers\CalculatorController::class, 'index'])->middleware(['confirmed'])->name('calculator');
Route::get('/converter', [App\Http\Controllers\ConverterController::class, 'index'])->name('converter');
Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile');
Route::post('/changeName', [App\Http\Controllers\ProfileController::class, 'changeName'])->name('profile.name.update');
Route::post('/changeGender', [App\Http\Controllers\ProfileController::class, 'changeGender'])->name('profile.gender.update');
Route::post('/changeUsername', [App\Http\Controllers\ProfileController::class, 'changeUsername'])->name('profile.username.update');
Route::post('/changePassword', [App\Http\Controllers\ProfileController::class, 'changePassword'])->name('profile.password.update');
Route::post('/changePhone', [App\Http\Controllers\ProfileController::class, 'changePhone'])->name('profile.phone.update');
// Route::post('/changeEmail', [App\Http\Controllers\ProfileController::class, 'changeEmail'])->name('profile.email.update');
Route::post('/changePartnershipCode', [App\Http\Controllers\ProfileController::class, 'changePartnershipCode'])->name('profile.partnership_code.update');
Route::post('/changeIsConfirmed', [App\Http\Controllers\ProfileController::class, 'changeIsConfirmed'])->name('profile.is_confirmed.update');
Route::post('/changeEmail', [App\Http\Controllers\ChangeEmailController::class, 'index'])->name('changeEmail');
Route::get('/receipt/{id}', [App\Http\Controllers\ReceiptController::class, 'index']);

Route::prefix('periods')->group(function () {
	Route::get('/', [App\Http\Controllers\PeriodsController::class, 'index'])->name('superadmin.periods');
	Route::post('/store', [App\Http\Controllers\PeriodsController::class, 'store'])->name('superadmin.periods.store');
	Route::post('/update/{id}', [App\Http\Controllers\PeriodsController::class, 'update'])->name('superadmin.periods.update');
	Route::post('/delete/{id}', [App\Http\Controllers\PeriodsController::class, 'destroy'])->name('superadmin.periods.delete');
});

Route::prefix('donations')->group(function () {
	Route::get('/', [App\Http\Controllers\DonationsController::class, 'index'])->name('superadmin.donations');
	Route::post('/', [App\Http\Controllers\DonationsController::class, 'filterByPeriod'])->name('superadmin.donations.filterByPeriod');
	Route::post('/store', [App\Http\Controllers\DonationsController::class, 'store'])->name('superadmin.donations.store');
	Route::post('/duplicate', [App\Http\Controllers\DonationsController::class, 'duplicate'])->name('superadmin.donations.duplicate');
	Route::post('/update/{id}', [App\Http\Controllers\DonationsController::class, 'update'])->name('superadmin.donations.update');
	Route::post('/delete/{id}', [App\Http\Controllers\DonationsController::class, 'destroy'])->name('superadmin.donations.delete');
	Route::post('/turnoff/{id}', [App\Http\Controllers\DonationsController::class, 'turnOff'])->name('superadmin.donations.turnoff');
	Route::post('/turnon/{id}', [App\Http\Controllers\DonationsController::class, 'turnOn'])->name('superadmin.donations.turnon');
	Route::get('/activateAll/{id}', [App\Http\Controllers\DonationsController::class, 'activateAll'])->name('superadmin.donations.activateAll');
	Route::get('/disableAll/{id}', [App\Http\Controllers\DonationsController::class, 'disableAll'])->name('superadmin.donations.disableAll');
});

Route::prefix('levels')->group(function () {
	Route::get('/', [App\Http\Controllers\LevelsController::class, 'index'])->name('superadmin.levels');
	Route::post('/update/{id}', [App\Http\Controllers\LevelsController::class, 'update'])->name('superadmin.levels.update');
});

Route::prefix('classes')->group(function () {
	Route::get('/', [App\Http\Controllers\ClassesController::class, 'index'])->name('superadmin.classes');
	Route::post('/store', [App\Http\Controllers\ClassesController::class, 'store'])->name('superadmin.classes.store');
	Route::post('/update/{id}', [App\Http\Controllers\ClassesController::class, 'update'])->name('superadmin.classes.update');
	Route::post('/delete/{id}', [App\Http\Controllers\ClassesController::class, 'destroy'])->name('superadmin.classes.delete');
});

Route::prefix('users')->group(function () {
	Route::get('/', [App\Http\Controllers\UsersController::class, 'index'])->name('superadmin.users');
	Route::post('/store', [App\Http\Controllers\UsersController::class, 'store'])->name('superadmin.users.store');
	Route::post('/update/{id}', [App\Http\Controllers\UsersController::class, 'update'])->name('superadmin.users.update');
	Route::post('/delete/{id}', [App\Http\Controllers\UsersController::class, 'destroy'])->name('superadmin.users.delete');
});

Route::prefix('volunteers')->group(function () {
	Route::get('/', [App\Http\Controllers\VolunteersController::class, 'index'])->name('admin.volunteers');
	Route::post('/store', [App\Http\Controllers\VolunteersController::class, 'store'])->name('admin.volunteers.store');
	Route::post('/update/{id}', [App\Http\Controllers\VolunteersController::class, 'update'])->name('admin.volunteers.update');
	Route::post('/delete/{id}', [App\Http\Controllers\VolunteersController::class, 'destroy'])->name('admin.volunteers.delete');
});

Route::prefix('recaps')->group(function () {
	Route::get('/', [App\Http\Controllers\RecapsController::class, 'index'])->name('admin.recaps');
	Route::post('/', [App\Http\Controllers\RecapsController::class, 'filterByPeriod'])->name('admin.recaps.filterByPeriod');
	Route::post('/store', [App\Http\Controllers\RecapsController::class, 'store'])->name('admin.recaps.store');
	Route::post('/update/{id}', [App\Http\Controllers\RecapsController::class, 'update'])->name('admin.recaps.update');
	Route::post('/delete/{id}', [App\Http\Controllers\RecapsController::class, 'destroy'])->name('admin.recaps.delete');
	Route::post('/export', [App\Http\Controllers\RecapsController::class, 'export'])->name('admin.recaps.export');
});

Route::prefix('resume')->middleware(['confirmed'])->group(function () {
	Route::get('/', [App\Http\Controllers\ResumeController::class, 'index'])->name('resume');
	Route::post('/', [App\Http\Controllers\ResumeController::class, 'filterByPeriod'])->name('resume.filterByPeriod');
	Route::get('/reports/{id}', [App\Http\Controllers\ResumeController::class, 'reports'])->name('resume.reports');
	Route::get('/distributions/{id}', [App\Http\Controllers\ResumeController::class, 'distributions'])->name('resume.distributions');
	Route::post('/export', [App\Http\Controllers\ResumeController::class, 'export'])->name('resume.export');
});

Route::prefix('ranks')->middleware(['confirmed'])->group(function () {
	Route::get('/', [App\Http\Controllers\RanksController::class, 'index'])->name('ranks');
	Route::post('/', [App\Http\Controllers\RanksController::class, 'filterByPeriod'])->name('ranks.filterByPeriod');
	Route::get('/show/{id}', [App\Http\Controllers\RanksController::class, 'show'])->name('ranks.show');
});

Route::prefix('media')->middleware(['confirmed'])->group(function () {
	Route::get('/', [App\Http\Controllers\MediaController::class, 'index'])->name('media');
	Route::get('/download/{id}', [App\Http\Controllers\MediaController::class, 'download'])->name('media.download');
	Route::post('/store', [App\Http\Controllers\MediaController::class, 'store'])->name('media.store');
	Route::post('/update/{id}', [App\Http\Controllers\MediaController::class, 'update'])->name('media.update');
	Route::post('/delete/{id}', [App\Http\Controllers\MediaController::class, 'destroy'])->name('media.delete');
});

Route::prefix('reports')->middleware(['confirmed'])->group(function () {
	Route::get('/', [App\Http\Controllers\ReportsController::class, 'index'])->name('volunteer.reports');
	Route::post('/', [App\Http\Controllers\ReportsController::class, 'filterByPeriod'])->name('volunteer.reports.filterByPeriod');
	Route::get('/report', [App\Http\Controllers\ReportsController::class, 'create'])->name('volunteer.reports.create');
	Route::get('/edit/{id}', [App\Http\Controllers\ReportsController::class, 'edit'])->name('volunteer.reports.edit');
	Route::get('/show/{id}', [App\Http\Controllers\ReportsController::class, 'show'])->name('volunteer.reports.show');
	Route::get('/download/{id}', [App\Http\Controllers\ReportsController::class, 'download'])->name('volunteer.reports.download');
	Route::post('/store', [App\Http\Controllers\ReportsController::class, 'store'])->name('volunteer.reports.store');
	Route::post('/update/{id}', [App\Http\Controllers\ReportsController::class, 'update'])->name('volunteer.reports.update');
	Route::post('/delete/{id}', [App\Http\Controllers\ReportsController::class, 'destroy'])->name('volunteer.reports.delete');
});

Route::prefix('certificate')->middleware(['confirmed'])->group(function () {
	Route::get('/', [App\Http\Controllers\CertificateController::class, 'index'])->name('certificate');
	Route::post('/show', [App\Http\Controllers\CertificateController::class, 'show'])->name('certificate.show');
	Route::post('/download', [App\Http\Controllers\CertificateController::class, 'download'])->name('certificate.download');
});