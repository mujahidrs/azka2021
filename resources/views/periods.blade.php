@extends('layouts.app')

@section('title', 'Periods')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <div class="col-md mb-2">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#addData">
          Add Period
        </button>
        <!-- Modal -->
        <div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Add Period</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form method="POST" action="{{ route("superadmin.periods.store") }}">
              @csrf
                  <div class="modal-body">
                      <div class="form-group">
                          <label>Period Name</label>
                          <input type="text" name="name" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>Start Date</label>
                          <input type="datetime-local" name="start_date" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>End Date</label>
                          <input type="datetime-local" name="end_date" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>Total Target</label>
                          <input type="number" name="total_target" class="form-control" value="0" min="0">
                      </div>
                      <table class="table">
                      <thead>
                          <tr>
                          <th class="text-center">Level</th>
                          <th class="text-center">Target</th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($levels as $key => $level)
                          <tr>
                          <td class="text-center">
                              {{ $level->id }}
                              <input type="hidden" name="level_id[]" class="form-control" value="{{ $level->id }}">
                          </td>
                          <td><input type="number" name="target[]" class="form-control" value="0" min="0"></td>
                          </tr>
                          @endforeach
                      </tbody>
                      </table>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </form>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md">
            
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @if(count($periods) === 0)
                        <div class="alert alert-danger" role="alert">
                            There are no data in this table
                        </div>
                    @else
                    <table id="example1" class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Target</th>
                                <th>Total Target</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Target</th>
                                <th>Total Target</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($periods as $key => $period)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $period->name }}</td>
                                <td>{{ $period->start_date }}</td>
                                <td>{{ $period->end_date }}</td>
                                <td>
                                  @foreach($period->targets as $target)
                                    @if($target->target != 0)
                                    <span>{{ $target->level_id }}: {{ $target->target }}</span>
                                    <br/>
                                    @endif
                                  @endforeach
                                </td>
                                <td>{{ $period->total_target }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editData-{{ $period->id }}"><i class="fa fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteData-{{ $period->id }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>

                            <!-- Modal -->
                            <div class="modal fade" id="editData-{{ $period->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Period</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="POST" action="{{ route("superadmin.periods.update", ["id"=>$period->id]) }}">
                                  @csrf
                                      <div class="modal-body">
                                        <div class="form-group">
                                            <label>Period Name</label>
                                            <input type="text" name="name" class="form-control" value="{{ $period->name }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Start Date</label>
                                            <input type="datetime-local" name="start_date" class="form-control" value="{{ date("Y-m-d", strtotime($period->start_date)) . "T" . date("H:i", strtotime($period->start_date)) }}">
                                        </div>
                                        <div class="form-group">
                                            <label>End Date</label>
                                            <input type="datetime-local" name="end_date" class="form-control" value="{{ date("Y-m-d", strtotime($period->end_date)) . "T" . date("H:i", strtotime($period->end_date)) }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Total Target</label>
                                            <input type="number" name="total_target" class="form-control" value="{{ $period->total_target }}" value="0" min="0">
                                        </div>
                                        <div class="m-3">
                                          <div class="row">
                                            <div class="col col-2 text-center font-weight-bold" style="padding: 12px; border-bottom: 2px solid #dee2e6; border-top: 1px solid #dee2e6;">
                                              Level
                                            </div>
                                            <div class="col col-10 text-center font-weight-bold" style="padding: 12px; border-bottom: 2px solid #dee2e6; border-top: 1px solid #dee2e6;">
                                              Target
                                            </div>
                                          </div>
                                          @foreach($period->targets as $key => $period_target)
                                            <div class="row">
                                              <div class="col col-2 text-center" style="padding: 12px; border-top: 1px solid #dee2e6;">
                                                {{ $period_target->level_id }}
                                                <input type="hidden" name="level_id[]" class="form-control" value="{{ $period_target->level_id }}">
                                              </div>
                                              <div class="col col-10" style="padding: 12px; border-top: 1px solid #dee2e6;"><input type="number" name="target[]" class="form-control" min="0" value="{{ $period_target->target }}"></div>
                                            </div>
                                          @endforeach
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="deleteData-{{ $period->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Delete Period</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="POST" action="{{ route("superadmin.periods.delete", ["id"=>$period->id]) }}">
                                  @csrf
                                      <div class="modal-body">
                                        Are you sure you want to delete this data?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                        <button type="submit" class="btn btn-primary">Yes</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
