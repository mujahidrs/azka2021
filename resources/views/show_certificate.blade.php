@extends('layouts.app')

@php
  $active_page = "Certificate";
@endphp

@section('title', 'Certificate')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('certificate.download') }}">
                        @csrf
                        <div class="form-group">
                            <label>Donor Name</label>
                            <input class="form-control" type="text" disabled value="{{ $name }}">
                        </div>
                        <div class="form-group">
                            <label>Confirm Donor Name</label>
                            <input class="form-control" type="text" name="donor_name" value="{{ $name }}">
                        </div>
                        <div class="form-group">
                            <label>Font Size (default: 36)</label>
                            <input class="form-control" type="number" min="0" name="font_size" value="36">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="fa fa-download"></i> Download Certificate
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
