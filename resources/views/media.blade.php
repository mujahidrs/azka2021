@extends('layouts.app')

@php
  $active_page = "Media";
@endphp


@section('title', 'Media')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md mb-2">
      @if(Auth::user()->role_id === 2)
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#addData">
          Add Media
        </button>
        <!-- Modal -->
        <div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Add Media</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form method="POST" action="{{ route("media.store") }}" enctype="multipart/form-data">
              @csrf
                  <div class="modal-body">
                      <input type="hidden" name="admin_id" value="{{ Auth::user()->id }}">
                      <div class="form-group">
                          <label>Title</label>
                          <input type="text" name="title" class="form-control">
                      </div>
                      <div class="form-group">
                          <label>Description</label>
                          <textarea name="desc" class="form-control"></textarea>
                      </div>
                      <div class="form-group">
                          <label>File</label>
                          <input type="file" name="file" class="form-control">
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </form>
              </div>
          </div>
        </div>
      @endif
    </div>
  </div>
  <div class="row justify-content-center">
    @if(count($media) === 0)
    <div class="col-md">
      <div class="card">
          <div class="card-body">
            <div class="alert alert-danger" role="alert">
                There are no media
            </div>

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
          </div>
        </div>
      </div>
    </div>
    @else
    @foreach($media as $key => $item)
      <div class="col col-md-4">
        <div class="card">
          <div class="card-header">
            {{ $item->title }}
          </div>
          <img src="{{ $item->file }}" class="card-img-top" height="300" width="300">
          <div class="card-body">
            <p class="card-text">{{ $item->desc }}</p>
            <small>{{ $item->date }}</small>
          </div>
          <div class="card-footer">
            <div class="btn-group btn-block">
              <a href="{{ route('media.download', ["id"=>$item->id]) }}" class="btn btn-primary">
                <i class="fa fa-download"></i> Download
              </a>
              @if(Auth::user()->role_id === 2)
              <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editData-{{ $item->id }}"><i class="fa fa-edit"></i> Edit</button>
              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteData-{{ $item->id }}"><i class="fa fa-trash"></i> Delete</button>
              @endif
            </div>
          </div>
        </div>
      </div>

      <!-- Modal -->
        <div class="modal fade" id="editData-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Media</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <form method="POST" action="{{ route("media.update", ["id"=>$item->id]) }}" enctype="multipart/form-data">
              @csrf
                  <div class="modal-body">
                      <input type="hidden" name="admin_id" value="{{ Auth::user()->id }}">
                      <div class="form-group">
                          <label>Title</label>
                          <input type="text" name="title" class="form-control" value="{{ $item->title }}">
                      </div>
                      <div class="form-group">
                          <label>Description</label>
                          <textarea name="desc" class="form-control">{{ $item->desc }}</textarea>
                      </div>
                      <div class="form-group">
                          <label>File</label>
                          <input type="file" name="file" class="form-control" value="{{ $item->file }}">
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </form>
              </div>
          </div>
        </div>

      <!-- Modal -->
      <div class="modal fade" id="deleteData-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Delete Media</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" action="{{ route("media.delete", ["id"=>$item->id]) }}">
            @csrf
                <div class="modal-body">
                  Are you sure you want to delete this data?
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    @endforeach
    @endif
  </div>
</div>
@endsection
