<?php

function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
  return $hasil_rupiah;
 
}

?>

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- FontAwesome -->
    <link rel="stylesheet" href="{{ asset('css/all.min.css') }}">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.bootstrap4.min.css') }}">
</head>
<body>
    <div class="container">
      <main>
        <div class="py-5 text-center">
          <img class="d-block mx-auto mb-4" src="{{ asset('images/logo.png') }}" alt="">
          <h2>Report #{{$reports[0]->invoice}}</h2>
        </div>

        <div class="row g-5">
          <div class="col-md-5 col-lg-4 order-md-last">
            <h4 class="d-flex justify-content-between align-items-center mb-3">
              <span class="text-primary">Donations</span>
              <span class="badge bg-primary rounded-pill">{{ count($reports) }}</span>
            </h4>
            <ul class="list-group mb-3">
                <?php $sum_nominal = 0; ?>
              @foreach($reports as $report)
                <li class="list-group-item d-flex justify-content-between lh-sm">
                    <div>
                      <h6 class="my-0">{{ $report->donation->name }}</h6>
                    </div>
                    <span class="text-muted">{{ rupiah($report->nominal) }}<?php $sum_nominal += $report->nominal ?></span>
                </li>
              @endforeach
              <li class="list-group-item d-flex justify-content-between">
                <span>Total</span>
                <strong>{{ rupiah($sum_nominal) }}</strong>
              </li>
            </ul>
          </div>
          <div class="col-md-7 col-lg-8">
            <table class="table">
                    <tr>
                      <th>Invoice</th>
                      <td>{{ $reports[0]->invoice }}</td>
                    </tr>
                    <tr>
                      <th>Volunteer Name</th>
                      <td>{{ $reports[0]->volunteer->name }}</td>
                    </tr>
                    <tr>
                      <th>Donor Name</th>
                      <td>{{ $reports[0]->donor->name }}</td>
                    </tr>
                    <tr>
                      <th>Donor Address</th>
                      <td>{{ $reports[0]->donor->address == null ? '-' : $reports[0]->donor->address }}</td>
                    </tr>
                    <tr>
                      <th>Donor Phone</th>
                      <td>{{ $reports[0]->donor->phone == null ? '-' : $reports[0]->donor->phone }}</td>
                    </tr>
                    <tr>
                      <th>Donor Email</th>
                      <td>{{ $reports[0]->donor->email == null ? '-' : $reports[0]->donor->email }}</td>
                    </tr>
                    <tr>
                      <th>Donor NPWP</th>
                      <td>{{ $reports[0]->donor->npwp == null ? '-' : $reports[0]->donor->npwp }}</td>
                    </tr>
                    <tr>
                      <th>Donation Date</th>
                      <td>{{ $reports[0]->date }}</td>
                    </tr>
                    <tr>
                      <th>Donation Type</th>
                      <td>{{ $reports[0]->type == 'transfer' ? 'Transfer' : 'Tunai' }}</td>
                    </tr>
                    <tr>
                      <th>Proof of Payment</th>
                      <td>
                        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#showImage"><img src="{{ $reports[0]->proof }}" class="img-thumbnail"></td></button>

                        <!-- Modal -->
                        <div class="modal fade" id="showImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Proof of Payment</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body text-center">
                                <img src="{{ $reports[0]->proof }}" class="img-fluid" width="500"></td>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                    </tr>
                  </table>
          </div>
        </div>
      </main>

      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; {{ date('Y') }} YBPMMD</p>
      </footer>
    </div>

    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/responsive.bootstrap4.min.js') }}"></script>

    <script type="text/javascript">
        $("#example1").DataTable({
          "responsive": true,
          "autoWidth": false,
        });
        $("#example2").DataTable({
          "responsive": true,
          "autoWidth": false,
          "paging": false,
          "ordering": false,
          "info": false,
          "bFilter": false
        });
    </script>
</body>
</html>
