<?php
use App\Custom\Archivos;
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
        #container{
            background-image: url('{{ Archivos::imagenABase64('images/background3.jpg') }}'); 
            width: 97%; 
            height: 97%; 
            background-size: 100% 100%;
            margin: auto;
        }

        #id_kwitansi{
            font-size: 24pt;
            color: white;
            position: relative;
            left: 60px;
            top: 0px;
            width: 225px;
            text-align: left;
        }

        #donatur{
            text-align: center;
            font-size: {{$font_size}}pt;
            color: white;
            font-weight: bold;
            position: relative;
            top: 200px;
            left: 238px;
            width: 526px;
        }
    </style>
</head>
<body>
    <div id="container">
        <div id="id_kwitansi">
            {{$invoice}}
        </div>
        <div id="donatur">
            {{$nama_lengkap_donatur}}
        </div>
    </div>
</body>
</html>