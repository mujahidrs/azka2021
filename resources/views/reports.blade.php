@extends('layouts.app')

@php
  $active_page = "Reports";
@endphp

@section('title', 'Reports')

@section('content')
<?php 

$achievement = 0; 

function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
  return $hasil_rupiah;
 
}
?>

<div class="container">
    <div class="row justify-content-center">
      <div class="col-md mb-2">
        <!-- Button trigger modal -->
        <a href="{{ route('volunteer.reports.create') }}" class="btn btn-success float-right">
          Report
        </a>
      </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if($selected_period == null)
                        <div class="alert alert-danger" role="alert">
                            There are no period in this day
                        </div>
                    @else
                    <form method="POST" action="{{ route("volunteer.reports.filterByPeriod") }}">
                      @csrf
                      <div class="row">
                        <div class="col col-8">
                          <div class="form-group">
                            <label>Select Period</label>
                            <select class="form-control" name="period_id">
                              @foreach($periods as $period)
                                <option value="{{ $period->id }}" {{ $selected_period->id == $period->id ? 'selected' : '' }}>{{ $period->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col col-4">
                          <div class="form-group">
                            <label class="text-white">Button</label>
                            <button type="submit" class="btn btn-success btn-block">Go</button>
                          </div>
                        </div>
                      </div>
                    </form>
                    @endif
                    
                    @if(count($reports) === 0)
                        <div class="alert alert-danger" role="alert">
                            There are no data in this table
                        </div>
                    @else
                    <table id="example1" class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Donor Name</th>
                                <th>Total Donations</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Donor Name</th>
                                <th>Total Donations</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($reports as $key => $report)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $report->date }}</td>
                                <td>{{ $report->donor->name }}</td>
                                <td>
                                  {{ rupiah($report->sum_nominal) }}
                                  <?php $achievement += $report->sum_nominal ?>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route("volunteer.reports.show", ["id"=>$report->invoice]) }}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                        <a href="{{ route("volunteer.reports.download", ["id"=>$report->invoice]) }}" class="btn btn-primary"><i class="fa fa-id-card"></i></a>
                                        <a href="{{ route("volunteer.reports.edit", ["id"=>$report->invoice]) }}" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteData-{{ $report->invoice }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>

                            <!-- Modal -->
                            <div class="modal fade" id="deleteData-{{ $report->invoice }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Delete Report</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="POST" action="{{ route("volunteer.reports.delete", ["id"=>$report->invoice]) }}">
                                  @csrf
                                      <div class="modal-body">
                                        Are you sure you want to delete this data?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                        <button type="submit" class="btn btn-primary">Yes</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            
                            @endforeach
                            
                        </tbody>
                    </table>
                    @endif
                    <h3>Total Achievement: {{ rupiah($achievement) }}</h3>

                    @if($target > 0)
                    <h3>Target: {{ rupiah($target) }}</h3>
                    <div class="progress">
                      <div class="progress-bar" role="progressbar" style="width: {{ round($achievement/$target*100,2) }}%;" aria-valuenow="{{ round($achievement/$target*100,2) }}" aria-valuemin="0" aria-valuemax="100">{{ round($achievement/$target*100,2) }}%</div>
                    </div>
                    <h3>Progress: {{ round($achievement/$target*100,2) }}%</h3>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
