@extends('layouts.app')

@php
  $active_page = "Reports";
@endphp

@section('title', 'Report')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md mb-2">
          <a href="{{ route("volunteer.reports") }}" class="btn btn-primary float-left">
            Back
          </a>
      </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                  @if(count($donations) === 0)
                  <div class="card-body">
                  <div class="alert alert-danger" role="alert">
                    Sorry, you cannot report donations at this time
                  </div>
                  </div>
                  @else
                <form method="POST" action="{{ route("volunteer.reports.store") }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  
                  @if (session('status'))
                      <div class="alert alert-danger" role="alert">
                          {{ session('status') }}
                      </div>
                  @endif
                  <div class="form-group">
                    <label>Select Period</label>
                    <input type='hidden' class='form-control' name='period_id' value={{ $selected_period->id }}>
                    <select class="form-control" name="period_id" disabled>                    
                      @foreach($periods as $period)
                        <option value="{{ $period->id }}" {{ $selected_period->id == $period->id ? 'selected' : '' }}>{{ $period->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Volunteer Name</label>
                    <input class="form-control" type="text" value="{{ Auth::user()->name }}" disabled>
                    <input class="form-control" type="hidden" name="volunteer_id" value="{{ Auth::user()->id }}">
                  </div>
                  <div class="form-group">
                    <label>Partnership Code</label>
                    <input class="form-control" type="text" value="{{ Auth::user()->class->code }}" disabled>
                  </div>
                  <div class="card">
                    <div class="card-header">
                      <div class="btn-group btn-block">
                        <button type="button" class="btn btn-primary" onclick="selectType('new')">New Donor</button>
                        <input type="hidden" name="donor_type" id="donor_type" value="new">
                        <button type="button" class="btn btn-secondary" onclick="selectType('saved')">Saved Donors</button>
                      </div>
                    </div>
                    <div class="card-body">
                      <div id="new_donor" class="d-block">
                        <div class="form-group">
                          <label>Donor Name <span class="text-red">*</span></label>
                          <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" value="{{ old('name') }}">

                          @error('name')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                          @enderror
                        </div>
                        <div class="form-group">
                          <label>Donor Address</label>
                          <input class="form-control" type="text" name="address">
                        </div>
                        <div class="form-group">
                          <label>Donor Phone</label>
                          <input class="form-control" type="text" name="phone">
                        </div>
                        <div class="form-group">
                          <label>Donor Email</label>
                          <input class="form-control" type="email" name="email">
                        </div>
                        <div class="form-group">
                          <label>Donor NPWP</label>
                          <input class="form-control" type="text" name="npwp">
                        </div>
                      </div>

                      <div id="reports" class="d-none">{{ json_encode($reports) }}</div>

                      <div id="saved_donor" class="d-none">
                        <div class="form-group">
                          <label>Donor Name <span class="text-red">*</span></label>
                          <select class="form-control" name="donor_id" onchange="selectDonor()" id="donor_id">
                            <option value="">Select Donor Name</option>
                            @foreach($reports as $key => $report)
                              <option value="{{ $report->donor_id }}">{{ $report->donor->name }}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label>Donor Address</label>
                          <input class="form-control" type="text" name="address" id="donor_address" disabled>
                        </div>
                        <div class="form-group">
                          <label>Donor Phone</label>
                          <input class="form-control" type="text" name="phone" id="donor_phone" disabled>
                        </div>
                        <div class="form-group">
                          <label>Donor Email</label>
                          <input class="form-control" type="email" name="email" id="donor_email" disabled>
                        </div>
                        <div class="form-group">
                          <label>Donor NPWP</label>
                          <input class="form-control" type="text" name="npwp" id="donor_npwp" disabled>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Donation Date <span class="text-red">*</span></label>
                    <input class="form-control @error('date') is-invalid @enderror" type="date" name="date" value="{{ date('Y-m-d') }}">

                    @error('date')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group" id="donation_type">
                    <label>Donation Type</label>
                    <select class="form-control" name="type" onchange="changeDonationType(event)">
                      <option value="transfer" selected>Transfer</option>
                      <option value="tunai">Cash</option>
                    </select>
                  </div>
                  <div id="donation_to_container">
                    <div class="form-group" id="donation_type_transfer">
                      <label>Donation To <span class="text-red">*</span></label>
                      <select class="form-control" name="donation_to">
                        <option disabled selected>-- Select Option --</option>
                        <option value="virtual">Virtual Account</option>
                        <option value="bank">Bank Account</option>
                      </select>
                    </div>
                  </div>
                  
                  
                  <table class="table" id="example2">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Donation</th>
                        <th>Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($donations as $key => $donation)
                        <tr>
                          <td>{{ $key+1 }}</td>
                          <td>
                            {{ $donation->name }}
                            <input class="form-control" type="hidden" name="donation_id[]" min="0" value="{{ $donation->id }}">
                            <div class="input-group mb-3">
                              <span class="input-group-text" id="basic-addon1">Rp</span>
                              <input class="form-control" type="number" name="nominal[]" min="0" value="0">
                            </div>
                          </td>
                          <td>
                            <textarea class="form-control" name="desc[]"></textarea>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <div class="form-group">
                    <label>Proof of Payment <span class="text-red">*</span></label>
                    <input class="form-control @error('proof') is-invalid @enderror" type="file" name="proof" value="{{ old('proof') }}">

                    @error('proof')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  
                </div>

                <button type="submit" class="btn btn-primary btn-lg m-3">Submit</button>
              </form>
              @endif
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  var reports = JSON.parse(document.getElementById("reports").innerText);

  function selectType(type){
    if(type === "new"){
      $("#new_donor")[0].classList.remove("d-none");
      $("#new_donor")[0].classList.add("d-block");
      $("#saved_donor")[0].classList.remove("d-block");
      $("#saved_donor")[0].classList.add("d-none");
      $("#donor_type").val("new");
    }
    if(type === "saved"){
      $("#new_donor")[0].classList.add("d-none");
      $("#new_donor")[0].classList.remove("d-block");
      $("#saved_donor")[0].classList.add("d-block");
      $("#saved_donor")[0].classList.remove("d-none");
      $("#donor_type").val("saved");
    }
  }

  function selectDonor(){
    let donor_id = document.getElementById("donor_id").value;
    let report = reports.filter(report=>report.donor_id == donor_id)[0];
    let donor_address = report.donor.address;
    let donor_phone = report.donor.phone;
    let donor_email = report.donor.email;
    let donor_npwp = report.donor.npwp;

    document.getElementById("donor_address").value = donor_address;
    document.getElementById("donor_phone").value = donor_phone;
    document.getElementById("donor_email").value = donor_email;
    document.getElementById("donor_npwp").value = donor_npwp;

  }

  function changeDonationType(e){
    console.log(e.target.value);

    if(e.target.value === 'transfer'){
      document.getElementById('donation_to_container').innerHTML = `
        <div class="form-group" id="donation_type_transfer">
          <label>Donation To <span class="text-red">*</span></label>
          <select class="form-control" name="donation_to">
            <option disabled selected>-- Select Option --</option>
            <option value="virtual">Virtual Account</option>
            <option value="bank">Bank Account</option>
          </select>
        </div>
      `;
    }
    else{
      document.getElementById('donation_to_container').innerHTML = `
      <input type="hidden" name="donation_to" value="tunai" id="donation_type_tunai">
      `;
    }
  }


</script>
@endsection