@extends('layouts.app')

@php
  $active_page = "Donations";
@endphp

@section('title', 'Donations')

@section('content')
<div class="container">
    <div class="row justify-content-center">    
      <div class="col-md mb-2 text-center">
        <!-- Button trigger modal -->
        <a href="{{ route('superadmin.donations.activateAll', ['id' => $selected_period->id]) }}" class="btn btn-primary">
          Activate All
        </a>
      </div>
      <div class="col-md mb-2 text-center">
        <!-- Button trigger modal -->
        <a href="{{ route('superadmin.donations.disableAll', ['id' => $selected_period->id]) }}" class="btn btn-warning">
          Disable All
        </a>
      </div>
      <div class="col-md mb-2 text-center">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#duplicateData">
          Duplicate Donations
        </button>
        <!-- Modal -->
        <div class="modal fade text-left" id="duplicateData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Duplicate Donations</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form method="POST" action="{{ route("superadmin.donations.duplicate") }}">
              @csrf
                  <div class="modal-body">
                    <div class="form-group">
                      <label>From</label>
                      <select name="duplicate_from" class="form-control">
                        <option value="">-- Select Option --</option>
                        @foreach($periods as $period)
                          <option value={{ $period->id }}>{{ $period->name }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="form-group">
                      <label>To</label>
                      <select name="duplicate_to" class="form-control">
                        @foreach($periods as $period)
                          <option value={{ $period->id }} {{ $period->id === $selected_period->id ? 'selected' : '' }}>{{ $period->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md mb-2 text-center">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addData">
          Add Donation
        </button>
        <!-- Modal -->
        <div class="modal fade text-left" id="addData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Donation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form method="POST" action="{{ route("superadmin.donations.store") }}">
              @csrf
                  <div class="modal-body">
                    <div class="form-group">
                        <label>Donation Name</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                      <label>Period</label>
                      <select name="period_id" class="form-control">
                        @foreach($periods as $period)
                          <option value={{ $period->id }} {{ $period->id === $selected_period->id ? 'selected' : '' }}>{{ $period->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if($selected_period == null)
                        <div class="alert alert-danger" role="alert">
                            There are no period in this day
                        </div>
                    @else
                    <form method="POST" action="{{ route("superadmin.donations.filterByPeriod") }}">
                      @csrf
                      <div class="row">
                        <div class="col col-8">
                          <div class="form-group">
                            <label>Select Period</label>
                            <select class="form-control" name="period_id">
                              @foreach($periods as $period)
                                <option value="{{ $period->id }}" {{ $selected_period->id == $period->id ? 'selected' : '' }}>{{ $period->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col col-4">
                          <div class="form-group">
                            <label class="text-white">Button</label>
                            <button type="submit" class="btn btn-success btn-block">Go</button>
                          </div>
                        </div>
                      </div>
                    </form>
                    @endif
                    
                    @if(count($donations) === 0)
                        <div class="alert alert-danger" role="alert">
                            There are no data in this table
                        </div>
                    @else
                    <table id="donations-table" class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($donations as $key => $donation)
                            <tr>
                                <td>{{ $donation->sort_number }}</td>
                                <td>{{ $donation->name }}</td>
                                <td>
                                  @if($donation->status === "active")
                                    <span class="badge badge-success">{{ $donation->status }}</span>
                                  @else
                                    <span class="badge badge-secondary">{{ $donation->status }}</span>
                                  @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        @if($donation->status === "inactive")
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#turnOnData-{{ $donation->id }}"><i class="fa fa-check"></i></button>
                                        @else
                                          <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#turnOffData-{{ $donation->id }}"><i class="fa fa-times"></i></button>
                                        @endif
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editData-{{ $donation->id }}"><i class="fa fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteData-{{ $donation->id }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>

                            <!-- Modal -->
                            <div class="modal fade" id="editData-{{ $donation->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Donation</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="POST" action="{{ route("superadmin.donations.update", ["id"=>$donation->id]) }}">
                                  @csrf
                                      <div class="modal-body">
                                        <div class="form-group">
                                            <label>Donation Name</label>
                                            <input type="text" name="name" class="form-control" value="{{ $donation->name }}">
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="deleteData-{{ $donation->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Delete Donation</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="POST" action="{{ route("superadmin.donations.delete", ["id"=>$donation->id]) }}">
                                  @csrf
                                      <div class="modal-body">
                                        Are you sure you want to delete this data?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                        <button type="submit" class="btn btn-primary">Yes</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="turnOffData-{{ $donation->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Turn Off Donation</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="POST" action="{{ route("superadmin.donations.turnoff", ["id"=>$donation->id]) }}">
                                  @csrf
                                      <div class="modal-body">
                                        Are you sure you want to turn off this donation?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                        <button type="submit" class="btn btn-primary">Yes</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="turnOnData-{{ $donation->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Turn On Donation</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="POST" action="{{ route("superadmin.donations.turnon", ["id"=>$donation->id]) }}">
                                  @csrf
                                      <div class="modal-body">
                                        Are you sure you want to turn on this donation?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                        <button type="submit" class="btn btn-primary">Yes</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
