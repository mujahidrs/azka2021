<?php
use App\Custom\Archivos;

function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
  return $hasil_rupiah;
 
}

$tanggal = date('d', strtotime($reports[0]->date));
$bulan = date('m', strtotime($reports[0]->date));

$months = [
	"Januari",
	"Februari",
	"Maret",
	"April",
	"Mei",
	"Juni",
	"Juli",
	"Agustus",
	"September",
	"Oktober",
	"November",
	"Desember"
];

$tahun = date('Y', strtotime($reports[0]->date));

$date = $tanggal . ' ' . $months[(int)$bulan-1] . ' ' . $tahun;
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		table {  
		    font-family: 'Arial'  
		}
		td {
			font-size: 20px;
		}
	</style>
</head>
<body>
	<table bgcolor="#f3ffff" width="100%">
	    <tbody>
	    	<tr>
	        	<th colspan="4" style="color: white; font-size: 40px;" bgcolor="#03786f" align="center">YAYASAN PEMIMPIN MUDA</th>
	    	</tr>
		    <tr>
		        <td width="200">
					<div style="color: white">Test</div>
					<div style="color: white">Test</div>
		            <img src="{{ Archivos::imagenABase64('images/Logo Laznas Yakesma.png') }}" width="70%">
		            <img src="{{ Archivos::imagenABase64('images/LOGO PMuda.png') }}" width="30%">
		        </td>
		        <td colspan="3" style="color: white; font-size: 30px; vertical-align: top;" bgcolor="#03786f">Sekretariat: <br style="font-size: 25px"><br>Jl. Pisangan Baru Utara Rt. 007 Rw. 012 No. 84 Kel. Pisangan Baru <br>Kec. Matraman Jakarta Timur<br style="font-size: 25px">www.pemimpinmuda.com</td>
		    </tr>
		    <tr>
		        <td colspan="2">
		        	<table width="100%">
		        		<tr>
		        			<td colspan="2" style="font-size: 35px">FORMULIR SETORAN DONASI (FSD)</td>
		        		</tr>
		        		<tr>
					        <td colspan="2" bgcolor="#95c3c1">Nomor: {{ $reports[0]->invoice }}</td>
					    </tr>
					    <tr>
					        <td colspan="2" bgcolor="#f3ffff">Bismillaahirrahmaanirrahiim,</td>
					    </tr>
					    <tr>
					        <td colspan="2" bgcolor="#f3ffff"><b>Telah Terima Dari</b></td>
					    </tr>
					    <tr>
					        <td colspan="2" bgcolor="#95c3c1">{{ $reports[0]->donor->name }}</td>
					    </tr>
					    <tr>
					        <td colspan="2" bgcolor="#f3ffff"><b>Alamat</b></td>
					    </tr>
					    <tr>
					        <td colspan="2" bgcolor="#95c3c1">{{ $reports[0]->donor->address == null ? '-' : $reports[0]->donor->address }}</td>
					    </tr>
					    <tr>
					        <td colspan="2" bgcolor="#f3ffff"><b>Email</b></td>
					    </tr>
					    <tr>
					        <td colspan="2" bgcolor="#95c3c1">{{ $reports[0]->donor->email == null ? '-' : $reports[0]->donor->email }}</td>
					    </tr>
					    <tr>
					        <td bgcolor="#f3ffff"><b>No. NPWP</b></td>
					        <td bgcolor="#f3ffff"><b>No. Tlp/HP</b></td>
					    </tr>
					    <tr>
					        <td bgcolor="#95c3c1">{{ $reports[0]->donor->npwp == null ? '-' : $reports[0]->donor->npwp }}</td>
					        <td bgcolor="#95c3c1">{{ $reports[0]->donor->phone == null ? '-' : $reports[0]->donor->phone }}</td>
					    </tr>
					    <tr>
					        <td bgcolor="#f3ffff"><b>Jenis Dana</b></td>
					        <td bgcolor="#95c3c1">{{ $reports[0]->type == 'transfer' ? 'Transfer' : 'Tunai' }}</td>
					    </tr>
					    <tr>
					        <td align="center">
					        	<img src="{{ Archivos::imagenABase64('images/'.$file_qr) }}" width="200">
					        </td>
					        <td bgcolor="#f3ffff" align="center"><img src="{{ Archivos::imagenABase64('images/doa.png') }}" width="450"></td>
					    </tr>
					    <tr>
					        <td colspan="2" bgcolor="#f3ffff" align="center">Jakarta, {{ $date }}</td>
					    </tr>
					    <tr>
					        <td bgcolor="#f3ffff" align="center">Donatur</td>
					        <td bgcolor="#f3ffff" align="center">Direktur Yayasan</td>
					    </tr>
					    <tr>
					    	<td></td>
					        <td bgcolor="#f3ffff" align="center"><img src="{{ Archivos::imagenABase64('images/ttd bang abd plus cap.png') }}" width="200"></td>
					    </tr>
					    <tr>
					        <td bgcolor="#f3ffff" align="center"><b>{{ $reports[0]->donor->name }}</b></td>
					        <td bgcolor="#f3ffff" align="center"><b>Abd. Mughni, S.Si</b></td>
					    </tr>
		        	</table>
		        </td>
		        <td colspan="2" style="vertical-align: top">
		        	<table width="100%">
		        		<tr>    			
					        <td style="color: white;" bgcolor="#03786f" align="center"><b>Jenis Donasi</b></td>
					        <td style="color: white;" bgcolor="#03786f" align="center"><b>Jumlah</b></td>
		        		</tr>
		        		<?php 
							$i = 0;
							$sum_nominal = 0;
						?>
		        		@foreach($reports as $key => $report)
		        			<tr>
		        				<td bgcolor="{{ $key % 2 == 0 ? '#f3ffff' : '#95c3c1' }}">{{ $key + 1 }}. {{ $report->donation->name }}</td>
		        				<td bgcolor="{{ $key % 2 == 0 ? '#f3ffff' : '#95c3c1' }}" align="right">
		        					{{ rupiah($report->nominal) }}
		        					<?php $sum_nominal += $report->nominal ?>
		        				</td>
		        			</tr>
		        		@endforeach
		        		<tr>    			
					        <td style="color: white;" bgcolor="#03786f" align="center">TOTAL</td>
		        			<td bgcolor="#f3ffff" align="right">{{ rupiah($sum_nominal) }}</td>
		        		</tr>
		        	</table>
					<br/>
					Keterangan :<br/>
					<ol>
						<li>Yakesma terdaftar sebagai lembaga Amil Zakat Nasional, mendapat izin dari Dirjen Pajak untuk menerbitkan Bukti Setor Zakat ( BSZ) sebagai pengurang Penghasilan Kena Pajak berdasarkan peraturan Dirjen Pajak No PER-05/PJ/2019</li>
						<li>Yakesma tidak menerima segala bentuk dana yang terkait dengan terorisme dan pencucian uang.</li>
						<li>Data dan dana zakat yang disetorkan oleh penyetor (Muzaki) telah sesuai dengan kriteria/syarat wajib zakat, yaitu: (1) Muslim, (2) Milik sempurna, (3) Cukup nisab, (4) Cukup haul, dan (5) Bersumber dari dana yang halal.</li>
					</ol>
		        </td>
		    </tr>
	    </tbody>
	</table>
</body>
</html>
