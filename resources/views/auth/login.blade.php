@extends('layouts.app')

@section('content')
<div class="page login-page">
    <div class="container d-flex align-items-center">
      <div class="form-holder has-shadow">
        <div class="row">
          <!-- Logo & Information Panel-->
          <div class="col-lg-6">
            <div class="info d-flex align-items-center">
              <div class="content">
                <div class="logo">
                  <h1>AZKA</h1>
                </div>
                <p>Sistem Pelaporan ZISWAF</p>
              </div>
            </div>
          </div>
          <!-- Form Panel -->
          <div class="col-lg-6 bg-white">
            <div class="form d-flex align-items-center">
              <div class="content">
                <form method="POST" action="{{ route('login') }}" class="form-validate">
                    @csrf
                
                    <div class="form-group">
                        <input id="login-username" type="text" class="input-material @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" autocomplete="username" autofocus>
                        <label for="login-username" class="label-material">{{ __('Username') }}</label>
                        
                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                
                    <div class="form-group">
                        <input id="login-password" type="password" class="input-material @error('password') is-invalid @enderror" name="password" autocomplete="current-password">
                        <label for="login-password" class="label-material">{{ __('Password') }}</label>
                        
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                
                    <div class="form-group">
                            <div class="form-check">
                                <input class="checkbox-template" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                
                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                    </div>
                
                    <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block shadow-lg" style="border-radius: 25px;">
                                {{ __('Login') }}
                            </button>            
                    </div>
                
                    <div class="form-group">
                            @if (Route::has('password.request'))
                                <a class="forgot-pass" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                
                            @if (Route::has('register'))
<!--                                 <br><small>Do not have an account? </small><a href="{{ route('register') }}" class="signup">Signup</a> -->
                            @endif
                            
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
