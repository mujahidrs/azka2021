<header class="page-header">
    <div class="container-fluid">
    <h2 class="no-margin-bottom">@yield('title')</h2>
    </div>
</header>

<!-- Breadcrumb-->
@if(Route::current()->getName() == 'home')
        
@else
<div class="breadcrumb-holder container-fluid">
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('home')}}"><i class="fa fa-tachometer"></i></a></li>
        <li class="breadcrumb-item active">@yield('title')</li>
    </ul>
</div>
@endif