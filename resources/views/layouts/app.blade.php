<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-material-admin/fontastic.css') }}">
    <!-- Google fonts - Poppins -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">

    <!-- Bootstrap Styles -->
    <link href="{{ asset('extras/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- FontAwesome -->
    <link rel="stylesheet" href="{{ asset('extras/font-awesome/css/font-awesome.min.css') }}">

    <!-- Theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-material-admin/style.green.css') }}" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap-material-admin/custom.css') }}">

    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.bootstrap4.min.css') }}">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>
<body>
    <div id="app">
        @auth
        <main>
            @include('layouts.header')

            <div class="page-content d-flex align-items-stretch"> 
                @include('layouts.sidebar')
    
                <div class="content-inner">
                    <!-- Page Header-->
                    @include('layouts.page-header')                
    
                    <!-- Content-->
                    <section style="background-color: #e0ffed;">
                        @yield('content')
                    </section>
    
                    <!-- Page Footer-->
                    @include('layouts.footer')
                </div>
            </div>
        </main>
        @else
            <!-- Content-->
            <main class="content">
                @yield('content')
            </main>
        @endauth
    </div>
    
    <!-- jQuery -->
    <script src="{{ asset('extras/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap 4 Bundle -->
    <script src="{{ asset('extras/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    
    <!-- Extra JavaScript files-->
    <script src="{{ asset('extras/popper.js/umd/popper.min.js') }}"> </script>
    <script src="{{ asset('extras/jquery.cookie/jquery.cookie.js') }}"> </script>
    <script src="{{ asset('extras/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('extras/jquery-validation/jquery.validate.min.js') }}"></script>

    <!-- Main File-->
    <script src="{{ asset('js/front.js') }}"></script>

    <!-- DataTables -->
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/responsive.bootstrap4.min.js') }}"></script>

    @yield('script')

    <script type="text/javascript">
        $("#example1").DataTable({
          "responsive": true,
          "autoWidth": false,
        });
        $("#example2").DataTable({
          "responsive": true,
          "autoWidth": false,
          "paging": false,
          "ordering": false,
          "info": false,
          "bFilter": false
        });
        $("#donations-table").DataTable({
          "responsive": true,
          "autoWidth": false,
          "pageLength": 50
        });
    </script>
</body>
</html>
