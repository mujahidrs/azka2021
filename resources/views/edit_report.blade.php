@extends('layouts.app')

@php
  $active_page = "Reports";
@endphp

@section('title', 'Edit Report')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <a href="{{ route("volunteer.reports") }}" class="btn btn-primary float-left">
                              Back
                            </a>
                        </div>
                        <div class="col text-center">
                            <h3>Edit Report</h3>
                        </div>
                        <div class="col">
                        </div>
                    </div>
                </div>

                <form method="POST" action="{{ route("volunteer.reports.update", ["id"=>$reports[0]->invoice]) }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  @if (session('status'))
                      <div class="alert alert-danger" role="alert">
                          {{ session('status') }}
                      </div>
                  @endif
                  <div class="form-group">
                    <label>Select Period</label>
                    <select class="form-control" name="period_id">
                      @foreach($periods as $period)
                        <option value="{{ $period->id }}" {{ $reports[0]->period_id == $period->id ? 'selected' : '' }}>{{ $period->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Volunteer Name</label>
                    <input class="form-control" type="text" value="{{ Auth::user()->name }}" disabled>
                    <input class="form-control" type="hidden" name="volunteer_id" value="{{ Auth::user()->id }}">
                    <input class="form-control" type="hidden" name="invoice" value="{{ $reports[0]->invoice }}">
                    <input class="form-control" type="hidden" name="donor_id" value="{{ $reports[0]->donor_id }}">
                  </div>
                  <div class="form-group">
                    <label>Donor Name</label>
                    <input class="form-control @error('name') is-invalid @enderror" type="text" name="name" value="{{ $reports[0]->donor->name }}">

                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label>Donor Address</label>
                    <input class="form-control" type="text" name="address" value="{{ $reports[0]->donor->address }}">
                  </div>
                  <div class="form-group">
                    <label>Donor Phone</label>
                    <input class="form-control" type="text" name="phone" value="{{ $reports[0]->donor->phone }}">
                  </div>
                  <div class="form-group">
                    <label>Donor Email</label>
                    <input class="form-control" type="email" name="phone" value="{{ $reports[0]->donor->email }}">
                  </div>
                  <div class="form-group">
                    <label>Donor NPWP</label>
                    <input class="form-control" type="text" name="npwp" value="{{ $reports[0]->donor->npwp }}">
                  </div>
                  <div class="form-group">
                    <label>Donation Date</label>
                    <input class="form-control @error('date') is-invalid @enderror" type="date" name="date" value="{{ $reports[0]->date }}">

                    @error('date')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label>Donation Type</label>
                    <select class="form-control" name="type">
                      <option value="transfer" {{ $reports[0]->type == 'transfer' ? 'selected' : '' }}>Transfer</option>
                      <option value="tunai" {{ $reports[0]->type == 'tunai' ? 'selected' : '' }}>Tunai</option>
                    </select>
                  </div>
                  <table class="table" id="example2">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Donation Name</th>
                        <th>Nominal</th>
                        <th>Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($reports as $key => $report)
                        <tr>
                          <td>{{ $key+1 }}</td>
                          <td>
                            {{ $report->donation->name }}
                            <input class="form-control" type="hidden" name="donation_id[]" min="0" value="{{ $report->donation_id }}">
                          </td>
                          <td>
                            <input class="form-control" type="number" name="nominal[]" min="0" value="{{ $report->nominal }}">
                          </td>
                          <td>
                            <textarea class="form-control" name="desc[]">{{ $report->desc }}</textarea>
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  <img src="{{ $reports[0]->proof }}" class="img-thumbnail">
                  <div class="form-group">
                    <label>Proof of Payment</label>
                    <input class="form-control @error('proof') is-invalid @enderror" type="file" name="proof" value="{{ old('proof') }}">

                    @error('proof')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
