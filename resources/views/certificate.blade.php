@extends('layouts.app')

@php
  $active_page = "Certificate";
@endphp


@section('title', 'Certificate')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(count($reports) === 0)
                        <div class="alert alert-danger" role="alert">
                            There are no data in this table
                        </div>
                    @else
                    <table id="example1" class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Donor Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Donor Name</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($reports as $key => $report)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $report->donor->name }}</td>
                                <td>
                                    <form method="POST">
                                    @csrf
                                        <input type="hidden" name="name" value="{{ $report->donor->name }}"> 

                                        <button formaction="{{ route('certificate.show') }}" class="btn btn-primary">
                                            <i class="fa fa-download"></i>
                                        </button>   
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
