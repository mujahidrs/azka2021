<table>
    <tr>
      <td colspan="2">Period</td>
      <td>: {{ $selected_period->name }}</td>
    </tr>
    <tr>
      <td colspan="2">Total Target</td>
      <td>: {{ $selected_period->total_target }}</td>
    </tr>
    <tr>
      <td colspan="2">Total Achievements</td>
      <td>: {{ $achievements }}</td>
    </tr>
    <tr>
      <td colspan="2">Percentage</td>
      <td>: {{ round($percentage, 2) }}%</td>
    </tr>
    <tr>
      <td colspan="2">Export Date</td>
      <td>: {{ $export_date }}</td>
    </tr>
  </table>
  <table>
    <thead>
      <tr>
        <th>No.</th>
        <th>Donation Name</th>
        <th>Income</th>
      </tr>
    </thead>
    <tbody>
      @foreach($donations as $key => $donation)
        <tr>
          <td class="text-center">{{ $key + 1 }}</td>
          <td class="text-center">{{ $donation->name }}</td>
          <td class="text-right">{{ $donation->income }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>