    <table>
      <tr>
        <td colspan="2">Period</td>
        <td colspan="2">: {{ $selected_period->name }}</td>
      </tr>
      <tr>
        <td colspan="2">Total Target</td>
        <td colspan="2">: {{ $selected_period->total_target }}</td>
      </tr>
      <tr>
        <td colspan="2">Total Achievements</td>
        <td colspan="2">: {{ $achievements }}</td>
      </tr>
      <tr>
        <td colspan="2">Percentage</td>
        <td colspan="2">: {{ round($percentage, 2) }}%</td>
      </tr>
      <tr>
        <td colspan="2">Export Date</td>
        <td colspan="2">: {{ $export_date }}</td>
      </tr>
    </table>
    <table>
      <thead>
        <tr>
          <th>No.</th>
          <th>Donation Date</th>
          <th>Donor Name</th>
          <th>Donor Phone</th>
          <th>Donor Email</th>
          <th>Donor NPWP</th>
          <th>Volunteer Name</th>
          <th>Donation Type</th>
          <th>Donation To</th>
          <th>Account Number</th>
          <th>Invoice</th>
          <th>Donation Name</th>
          <th>Nominal</th>
          <th>Proof of Payment</th>
          <th>Donor Address</th>
          <th>Partnership Code</th>
        </tr>
      </thead>
      <tbody>
        @foreach($reports as $key => $report)
          <tr>
            <td class="text-center">{{ $key + 1 }}</td>
            <td class="text-center">{{ $report->date }}</td>
            <td class="text-center">{{ $report->donor->name }}</td>
            <td class="text-center">{{ $report->donor->phone }}</td>
            <td class="text-center">{{ $report->donor->email }}</td>
            <td class="text-center">{{ $report->donor->npwp }}</td>
            <td class="text-center">{{ $report->volunteer?->name }}</td>
            <td class="text-center">{{ ucwords($report->type) }}</td>
            <td class="text-center">
              @if($report->donation_to === "virtual")
                Virtual Account
              @else
                Bank Account
              @endif
            </td>
            <td class="text-center">
              @if($report->donation_to === "virtual")
                9235200099900515
              @else
                7188899921
              @endif
            </td>
            <td class="text-center">{{ $report->invoice }}</td>
            <td class="text-center">{{ $report->donation->name }}</td>
            <td class="text-center">{{ $report->nominal }}</td>
            <td class="text-center">
              <a href="{{ env('APP_URL') . $report->proof }}">Open in Browser</a>
            </td>
            <td class="text-center">{{ $report->donor->address }}</td>
            <td class="text-center">{{ $report->volunteer?->class?->code }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>