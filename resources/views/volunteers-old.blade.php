@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <a href="{{ route("home") }}" class="btn btn-primary float-left">
                              Back
                            </a>
                        </div>
                        <div class="col text-center">
                            <h3>Volunteers</h3>
                        </div>
                        <div class="col">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#addData">
                              Add
                            </button>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Volunteer</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form method="POST" action="{{ route("admin.volunteers.store") }}">
                          @csrf
                              <div class="modal-body">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" name="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" name="username" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Gender</label>
                                    <select name="gender" class="form-control">
                                      <option value="male">Male</option>
                                      <option value="female">Female</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <div class="input-group mb-3">
                                      <span class="input-group-text" id="basic-addon1">+62</span>
                                      <input type="number" name="phone" class="form-control" placeholder="Phone" aria-label="Phone" aria-describedby="basic-addon1">
                                    </div>
                                </div>
                                <input type="hidden" name="role_id" value="3">
                                <div class="form-group">
                                    <label>Class</label>
                                    <select name="class_id" class="form-control">
                                      @foreach($classes as $key => $class)
                                        <option value="{{ $class->id }}">{{ $class->code }}</option>
                                      @endforeach
                                    </select>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                              </div>
                          </form>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @if(count($users) === 0)
                        <div class="alert alert-danger" role="alert">
                            There are no data in this table
                        </div>
                    @else
                    <table id="example1" class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Gender</th>
                                <th>Phone</th>
                                <th>Class</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Username</th>
                                <th>Gender</th>
                                <th>Phone</th>
                                <th>Class</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($users as $key => $user)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $user->name }}</td>
                                <td><a class="btn btn-secondary" href="mailto:{{ $user->email }}"><i class="fas fa-envelope"></i></a></td>
                                <td>{{ $user->username }}</td>
                                <td>{{ $user->gender }}</td>
                                <td><a class="btn btn-success" href="http://wa.me/{{ $user->phone }}"><i class="fab fa-whatsapp"></i></a></td>
                                <td>{{ $user->class->code }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editData-{{ $user->id }}"><i class="fas fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteData-{{ $user->id }}"><i class="fas fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>

                            
                            <!-- Modal -->
                            <div class="modal fade" id="editData-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Volunteer</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="POST" action="{{ route("admin.volunteers.update", ["id"=>$user->id]) }}">
                                  @csrf
                                      <div class="modal-body">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" name="name" class="form-control" value="{{ $user->name }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" name="email" class="form-control" value="{{ $user->email }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" name="username" class="form-control" value="{{ $user->username }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Gender</label>
                                            <select name="gender" class="form-control">
                                              <option value="male" {{ $user->gender == 'male' ? 'selected' : '' }}>Male</option>
                                              <option value="female" {{ $user->gender == 'female' ? 'selected' : '' }}>Female</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Phone</label>
                                            <div class="input-group mb-3">
                                              <span class="input-group-text" id="basic-addon1">+62</span>
                                              <input type="number" name="phone" class="form-control" placeholder="Phone" aria-label="Phone" aria-describedby="basic-addon1" value="{{ $user->phone }}">
                                            </div>
                                        </div>
                                        <input type="hidden" name="role_id" value="3">
                                        <div class="form-group">
                                            <label>Class</label>
                                            <select name="class_id" class="form-control">
                                              @foreach($classes as $key => $class)
                                                <option value="{{ $class->id }}" {{ $user->class_id == $class->id ? 'selected' : '' }}>{{ $class->code }}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="deleteData-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Delete Volunteer</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="POST" action="{{ route("admin.volunteers.delete", ["id"=>$user->id]) }}">
                                  @csrf
                                      <div class="modal-body">
                                        Are you sure you want to delete this data?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                        <button type="submit" class="btn btn-primary">Yes</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
